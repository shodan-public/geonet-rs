use colored::Colorize;
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::BTreeMap;
use structopt::StructOpt;
use ureq::Error;

const EXIT_ERROR_CODE: i32 = 1;

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Response {
    Success(DnsResult),
    Error { error: String },
}

// Define struct as can't use enum Response::Error as return type
#[derive(Serialize, Deserialize, Debug)]
struct APIError {
    detail: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct DnsResult {
    answers: Vec<DnsAnswer>,
    from_loc: Location,
}

#[derive(Serialize, Deserialize, Debug)]
struct DnsAnswer {
    r#type: String,
    value: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Location {
    city: String,
    country: String,
    latlon: String,
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "geodns",
    about = "Lookup a DNS record from different locations around the world"
)]
struct Cli {
    /// Output format (shell or json)
    #[structopt(default_value = "shell", short, long)]
    output: String,

    /// Record type to query
    #[structopt(default_value = "A", short = "t", long = "type")]
    rtype: String,

    /// Hostname/ domain to get information for
    fqdn: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();

    // Send the GeoNet API request and grab the response
    let url = format!(
        "https://geonet.shodan.io/api/geodns/{}?rtype={}",
        args.fqdn, args.rtype
    );
    let resp = ureq::get(&url.to_string()).call();

    let results: Vec<Response> = match resp {
        Ok(response) => response.into_json()?,
        Err(Error::Status(_, response)) => {
            let resp_str = response.into_string()?;
            let error: Result<APIError, serde_json::Error> =
                serde_json::from_str::<APIError>(&resp_str);

            match error {
                Ok(error) => {
                    // API return defined error response
                    println!("{}: {}", "Error".red().bold(), error.detail);
                }
                Err(_) => {
                    // Failed to parse, which are server errors, e.g. 503 Service Unavailable
                    println!(
                        "{}: DNS lookup failed, please try again later!",
                        "Error".red().bold()
                    );
                }
            };
            std::process::exit(EXIT_ERROR_CODE);
        }
        Err(_) => {
            // Some kind of io/transport error
            println!(
                "{}: DNS lookup failed, please try again later!",
                "Error".red().bold()
            );
            std::process::exit(EXIT_ERROR_CODE);
        }
    };
    let dnsresults: Vec<&DnsResult> = results
        .iter()
        .filter_map(|response| match response {
            Response::Success(result) => Some(result),
            _ => None,
        })
        .collect::<Vec<_>>();

    if dnsresults.is_empty() {
        println!("{}: Invalid hostname", "Error".red().bold());
        std::process::exit(EXIT_ERROR_CODE);
    }

    // For JSON output we don't need to do any additional processing
    if args.output == "json" {
        for result in dnsresults.iter() {
            println!("{}", serde_json::to_string(*result)?)
        }
    } else {
        // Group the results by value so it's easier to tell which locations
        // are getting different DNS results.
        let mut dnsmap: BTreeMap<&String, Vec<&Location>> = BTreeMap::new();
        for result in dnsresults.iter() {
            for answer in result.answers.iter() {
                // Initialize the vector for the key if it doesn't yet exist
                if !dnsmap.contains_key(&answer.value) {
                    dnsmap.insert(&answer.value, Vec::new());
                }

                // The key should always exist since we're checking for it right above
                let vec = dnsmap.get_mut(&answer.value).ok_or("Key doesn't exist")?;
                vec.push(&result.from_loc);
            }
        }

        // We got some DNS answers
        if dnsmap.len() > 0 {
            for (answer, locations) in &dnsmap {
                println!(
                    "{:30} {}",
                    answer.bold().white(),
                    &locations[0].city.dimmed()
                );

                if locations.len() > 1 {
                    for location in &locations[1..] {
                        println!("{:30} {}", "".to_string(), location.city.dimmed());
                    }
                    println!();
                }
            }
        } else {
            println!("No results found");
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use assert_cmd::Command;

    #[test]
    fn valid_query() {
        let mut cmd = Command::cargo_bin("geodns").unwrap();
        let output = cmd.arg("shodan.io").unwrap();

        assert!(!String::from_utf8(output.stdout).unwrap().contains("Error:"));

        cmd = Command::cargo_bin("geodns").unwrap();
        let output = cmd
            .arg("shodan.io")
            .arg("--type")
            .arg("A")
            .arg("--output")
            .arg("jsonnnn") // Fallback to shell output
            .unwrap();

        assert!(!String::from_utf8(output.stdout).unwrap().contains("Error:"));

        cmd = Command::cargo_bin("geodns").unwrap();
        let output = cmd
            .arg("shodan.io")
            .arg("--type")
            .arg("AAAA")
            .arg("--output")
            .arg("json")
            .unwrap();

        let output_str = String::from_utf8(output.stdout).unwrap();
        assert!(output_str.contains("{\"answers\":[{"));
        assert!(output_str.contains("\"country\":\""));
    }

    #[test]
    #[should_panic(expected = "Error: Invalid hostname")]
    fn invalid_hostname() {
        Command::cargo_bin("geodns").unwrap().arg("shodan").unwrap();
    }

    #[test]
    #[should_panic(expected = "Error: Unsupported DNS record type")]
    fn invalid_dns_type() {
        Command::cargo_bin("geodns")
            .unwrap()
            .arg("shodan.io")
            .arg("--type")
            .arg("AA")
            .unwrap();
    }
}
